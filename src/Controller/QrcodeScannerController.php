<?php

namespace Drupal\qrcode_scanner\Controller;

use Drupal\Core\StringTranslation\StringTranslationTrait;

class QrcodeScannerController {
  use StringTranslationTrait;

  public function initialisation() {
    $title = $this->t('QR-Code Scanner');

    $build['myelement'] = [
      '#theme' => 'qrcode_scanner_initialisation',
      '#title' => $title,
    ];

    $build['myelement']['#attached']['library'][] = 'qrcode_scanner/qrcode_scanner.scanner';

    return $build;
  }

}
